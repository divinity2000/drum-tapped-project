﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TileManager : MonoBehaviour
{
    public GameObject gameOver;
    public LayoutElement tilesGroup;
    public Text scoreText;
    public Text recordText;

    private int currentDeactive = 0;
    private List<GameObject> tilesQueue = new List<GameObject>();

    public float speedTimerDown = 1.0f;
    public float[] times = { 1.5f, 1.3f, 1.1f, 0.9f, 0.7f, 0.5f, 0.3f, 0.1f};
    private float[] tempTimes = { 0, 0, 0, 0, 0, 0, 0, 0 };
    private float tempTimer;

    public int score = 0;
    public int record = 0;

    private bool start = false;
    public bool timeCanDown = true;

    /// <summary>
    /// Функция вызывается перед первым обновлением кода
    /// (перед первым кадром по сути)
    /// Эта функция вызывает SetTime()
    /// Записывает начальный массив времени,
    /// восстанавливает из памяти рекорд и выводит его
    /// через компонент UI.Text
    /// API PlayerPrefs тут: https://docs.unity3d.com/ru/530/ScriptReference/PlayerPrefs.html
    /// Про Text читать тут: https://docs.unity3d.com/ru/current/Manual/script-Text.html
    /// API Text тут: https://docs.unity3d.com/ru/530/ScriptReference/UI.Text.html
    /// Про Start() тут: https://docs.unity3d.com/ru/530/Manual/ExecutionOrder.html
    /// </summary>
    void Start()
    {
        SetTime();
        tempTimer = tempTimes[0];
        record = PlayerPrefs.GetInt("record", 0);
        recordText.text = "Record:   " + record;
    }

    /// <summary>
    /// Функция вызывается каждый кадр
    /// В ней обрабатываюся события, отслеживающие
    /// количество отключенных плиток, изменение
    /// таймера, измение всего временного массива и
    /// событие проигрыша и проверка на выход из приложения
    /// Про IEnumerator тут: https://msdn.microsoft.com/ru-ru/library/system.collections.ienumerator(v=vs.110).aspx
    /// Про Time читать тут: https://docs.unity3d.com/ru/530/Manual/TimeFrameManagement.html
    /// API Time тут: https://docs.unity3d.com/ru/530/ScriptReference/Time.html
    /// API Application тут: https://docs.unity3d.com/ru/530/ScriptReference/Application.html
    /// Про Input читаем тут: https://docs.unity3d.com/ru/530/Manual/script-StandaloneInputModule.html
    /// API Input тут: https://docs.unity3d.com/ru/530/ScriptReference/Input.html
    /// Про Update() тут: https://docs.unity3d.com/ru/530/Manual/ExecutionOrder.html
    /// </summary>
    void Update()
    {
        if (start)
        {
            if (currentDeactive == 9)
                TileRespawn();

            if (tempTimer > 0)
                tempTimer -= Time.deltaTime;
            else
                TileRespawn();

            if (timeCanDown) // Да, это необходимый костыль
                StartCoroutine(TimeDowner());

            if (currentDeactive == 0)
                GameStoped();
        }

        if (Application.platform == RuntimePlatform.Android && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.Menu)))
            Application.Quit();
    }

    /// <summary>
    /// Тут все просто: активируем первый тайл
    /// в очереди, затем удаляем его из очереди
    /// и меням таймер, уменьшая итератор
    /// </summary>
    void TileRespawn()
    {
        tilesQueue[0].SetActive(true);
        tilesQueue.Remove(tilesQueue[0]);
        tempTimer = tempTimes[--currentDeactive];
    }

    /// <summary>
    /// По первому тапу на тайл начинаем игру,
    /// добавляем тайл в конец очереди,
    /// задаем нужный таймер,
    /// увеличиваем итератор и
    /// выводим счет
    /// Про Text читать тут: https://docs.unity3d.com/ru/current/Manual/script-Text.html
    /// API Text тут: https://docs.unity3d.com/ru/530/ScriptReference/UI.Text.html
    /// </summary>
    /// <returns>The tapped.</returns>
    /// <param name="tile">Tile.</param>
    public void Tapped(GameObject tile)
    {
        if (!start)
            start = true;
        tilesQueue.Add(tile);
        tempTimer = tempTimes[currentDeactive];
        currentDeactive++;
        scoreText.text = "Score:   " + (++score);
    }

    /// <summary>
    /// Каждую секунду изменяем весь 
    /// массив таймеров
    /// Про IEnumerator тут: https://msdn.microsoft.com/ru-ru/library/system.collections.ienumerator(v=vs.110).aspx
    /// </summary>
    /// <returns>The downer.</returns>
    public IEnumerator TimeDowner()
    {
        timeCanDown = false; //Костыль, который вроде как необходим
        yield return new WaitForSeconds(1);
        if (start && tempTimes[2] >= 0.1f)
        {
            for (int i = 0; i < tempTimes.Length; i++)
            {
                float _t = tempTimes[i];
                if (_t > .1f)
                    _t -= .01f * speedTimerDown;
                else if (_t < .05f)
                    _t = .05f;
                tempTimes[i] = _t;
            }
            timeCanDown = true;
        }
    }

    /// <summary>
    /// Вызывается когда игра окончена,
    /// останавливает работу интерфеса,
    /// возвращает стандартные значения
    /// и выводит всю информацию
    /// Про IEnumerator тут: https://msdn.microsoft.com/ru-ru/library/system.collections.ienumerator(v=vs.110).aspx
    /// API PlayerPrefs тут: https://docs.unity3d.com/ru/530/ScriptReference/PlayerPrefs.html
    /// Про Text читать тут: https://docs.unity3d.com/ru/current/Manual/script-Text.html
    /// API Text тут: https://docs.unity3d.com/ru/530/ScriptReference/UI.Text.html
    /// Про управление игровыми объектами читаем тут: https://docs.unity3d.com/ru/530/Manual/ControllingGameObjectsComponents.html
    /// API GetComponent читаем тут: https://docs.unity3d.com/ru/530/ScriptReference/GameObject.GetComponent.html
    /// </summary>
    public void GameStoped()
    {
        StopCoroutine(TimeDowner());
        SetTime();
        tempTimer = tempTimes[currentDeactive];
        start = false;
        timeCanDown = true;
        scoreText.text = "Score:   0";
        if (score > record)
        {
            PlayerPrefs.SetInt("record", score);
            recordText.text = "Record:   " + score;
        }
        tempTimer = currentDeactive = score = 0;
        gameOver.SetActive(true);

        if (tilesQueue.Count != 0)
        {
            foreach (GameObject tile in tilesQueue)
            {
                tile.SetActive(true);
            }
            tilesQueue.Clear();
        }
    }

    /// <summary>
    /// Возвращает массив таймеров к
    /// первоначальному состоянию
    /// </summary>
    void SetTime()
    {
        int idx = 0;
        while (idx != times.Length)
        {
            tempTimes[idx] = times[idx];
            idx++;
        }
    }
}
