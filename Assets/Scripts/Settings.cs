﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// Класс настроек
/// Не имеет ни стыда, ни жалости
/// Про сереализацию от юнити читаем тут: https://docs.unity3d.com/ru/530/Manual/script-Serialization.html
/// Про сериализацию от дотнета (System) читаем тут: https://msdn.microsoft.com/en-us/library/ms973893.aspx
/// API List тут: https://msdn.microsoft.com/en-us/library/6sh2ey19(v=vs.110).aspx
/// Все по UI тут: https://docs.unity3d.com/ru/530/Manual/UISystem.html
/// Все API UI тут: https://docs.unity3d.com/ru/530/ScriptReference/UI.AnimationTriggers.html тут ветка UnityEngine.UI
/// остальное все из текста понятно
/// </summary>
public class Settings : MonoBehaviour
{
    public List<Image> panels = new List<Image>(); 
    public List<Text> texts = new List<Text>();
    public Image tilesGroup;
    public List<Image> tiles = new List<Image>();

    [SerializeField]
    public List<ColorScheme> colorScheme = new List<ColorScheme>();
    [SerializeField]
    public List<TilesColor> tilesColor = new List<TilesColor>();

    public List<Image> divs = new List<Image>();

    public ToggleGroup toggleGroup;
    public List<Toggle> toggles = new List<Toggle>();

    void Start()
    {
        if (PlayerPrefs.GetInt("scheme", 0) == 0)
            SelectWhite();
        else
            SelectDark();

        int colorIdx = PlayerPrefs.GetInt("color", 0); 

        SelectColor(colorIdx);
        toggles[colorIdx].isOn = true; 
    }

    public void SelectWhite()
    {
        foreach (Image panel in panels)
            panel.color = colorScheme[0].panel;

        foreach (Text text in texts)
            text.color = colorScheme[0].text;

        foreach (Image div in divs)
            div.color = colorScheme[0].text;

        PlayerPrefs.SetInt("scheme", 0);
    }

    public void SelectDark()
    {
        foreach (Image panel in panels)
            panel.color = colorScheme[1].panel;

        foreach (Text text in texts)
            text.color = colorScheme[1].text;

        foreach (Image div in divs)
            div.color = colorScheme[1].text;

        PlayerPrefs.SetInt("scheme", 1);
    }

    public void SelectColor(int index)
    {
        tilesGroup.color = tilesColor[index].tilesGroup;
        foreach (Image tile in tiles)
            tile.color = tilesColor[index].tiles;

        PlayerPrefs.SetInt("color", index);
    }
}

[System.Serializable]
public struct TilesColor
{
    public Color tiles;
    public Color tilesGroup;
}

[System.Serializable]
public struct ColorScheme
{
    public Color panel;
    public Color text;
}