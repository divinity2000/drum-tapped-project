﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Класс нашей кнопочки
/// Вешается на нее же
/// Прост как тапоче
/// </summary>
public class Tile : MonoBehaviour
{
    /// <summary>
    /// Простой интерфейс который отключает
    /// зависимости компоновки тайлов(кнопочек)
    /// по обращению к LayoutElement
    /// Про IEnumerator тут: https://msdn.microsoft.com/ru-ru/library/system.collections.ienumerator(v=vs.110).aspx
    /// Про Layout читать тут: https://docs.unity3d.com/ru/530/Manual/UIAutoLayout.html
    /// API LayoutElement тут: https://docs.unity3d.com/ru/530/ScriptReference/UI.LayoutElement.html
    /// Про управление игровыми объектами читаем тут: https://docs.unity3d.com/ru/530/Manual/ControllingGameObjectsComponents.html
    /// API GetComponent читаем тут: https://docs.unity3d.com/ru/530/ScriptReference/GameObject.GetComponent.html
    /// </summary>
    /// <returns>The ignore.</returns>
    IEnumerator StartIgnore()
    {
        yield return new WaitForSeconds(0.01f);
        GetComponent<UnityEngine.UI.LayoutElement>().ignoreLayout = true;
    }

    /// <summary>
    /// Функция вызывается перед первым обновлением кода
    /// (перед первым кадром по сути)
    /// И эта функция вызывает наш интерфес
    /// Про Start() тут: https://docs.unity3d.com/ru/530/Manual/ExecutionOrder.html
    /// </summary>
    void Start()
    {
        StartCoroutine(StartIgnore());
    }

    /// <summary>
    /// Функция которая вызывается по нажатию на тайл
    /// Само нажатие отслеживается компонентом UI.Button
    /// Про Button читать тут: https://docs.unity3d.com/ru/530/Manual/script-Button.html
    /// API Button тут: https://docs.unity3d.com/ru/530/ScriptReference/UI.Button.html 
    /// Внутри мы вызываем функцию класса TileManager
    /// И отключаем объект
    /// Про управление игровыми объектами читаем тут: https://docs.unity3d.com/ru/530/Manual/ControllingGameObjectsComponents.html
    /// API GameObject.SetActive тут: https://docs.unity3d.com/ru/530/ScriptReference/GameObject.SetActive.html
    /// </summary>
    /// <param name="tileManager">
    /// Передаем в парамер класс манагера
    /// По сути замена GetComponent
    /// </param>
    public void Tap(TileManager tileManager)
    {
        tileManager.Tapped(gameObject); // Передаем текущай объект (this.gameObject)
        gameObject.SetActive(false);
    }
}
